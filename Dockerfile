FROM alpine:latest

LABEL maintainer="Mark Determan<mark@determan.io>"

ARG REF_SLUG
ARG BUILD_DATE

# Metadata
LABEL org.label-schema.vcs-ref=$REF_SLUG \
      org.label-schema.vcs-url="https://github.com/mdeterman/docker/alpine-zip" \
      org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.docker.dockerfile="/Dockerfile"

RUN apk add --update zip